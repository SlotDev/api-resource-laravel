<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// List Articles
Route::get('articles', 'ArticleController@index');
// List single Article
Route::get('article/{id}', 'ArticleController@show');
// Create new Article
Route::post('article', 'ArticleController@store');
// Update Article
Route::put('article/{id}', 'ArticleController@store');
// Delete Article
Route::delete('article/{id}', 'ArticleController@destroy');

